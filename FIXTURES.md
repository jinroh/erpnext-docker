# First steps
1. Clone Frappe docker repo to gitops folder  
  `git clone https://github.com/frappe/frappe_docker.git gitops`
2. Create a `.env` file in the **root project folder** (not the gitops folder!). See .env.example for stored variables
3. Start **Script sequence**

# Script sequence
1. `./cp-env.sh`  
  Copy the `.env` file from root folder to `gitops/` folder

2. `./create-docker-compose.sh`  
  Build fully production `docker-compose.yml` from partial compose files

3. `./docker-compose-up.sh`
  Bring your docker-compose up running

4. `./setup-erpnext.sh`  
  Install a fresh erpnext instance



  docker-compose -f docker-compose.yml exec -u0 frontend \
  sed -i 's/navigator.onLine/navigator.onLine||true/' \
  /usr/share/nginx/html/assets/js/desk.min.js \
  /usr/share/nginx/html/assets/js/dialog.min.js \
  /usr/share/nginx/html/assets/js/frappe-web.min.js

# Helpful Post
https://discuss.erpnext.com/t/runtimeerror-required-environment-variable-db-host-not-set/88624/9

use this post as entrypoint