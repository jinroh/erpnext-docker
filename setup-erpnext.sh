#!/bin/sh
export $(grep -v '^#' .env | xargs)

# echo docker-compose -f docker-compose.yml exec backend bench new-site $FRAPPE_SITE_NAME_HEADER --mariadb-root-password $DB_PASSWORD --admin-password $ERPNEXT_ADMIN_PASSWORT --install-app erpnext
cd gitops && docker-compose -f docker-compose.yml exec backend bench new-site $FRAPPE_SITE_NAME_HEADER --mariadb-root-password $DB_PASSWORD --admin-password $ERPNEXT_ADMIN_PASSWORT --install-app erpnext

unset $(grep -v '^#' .env | sed -E 's/(.*)=.*/\1/' | xargs)